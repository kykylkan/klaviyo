
<form method="post" action="{{ $action }}">
    @csrf
    @include('partials._input_text', ['inputName' => 'last_name'])
    @include('partials._input_text', ['inputName' => 'first_name'])
    @include('partials._input_text', ['inputName' => 'phone'])
    @include('partials._input_text', ['inputName' => 'email', 'type' => 'email'])

    <br>
    <button type="submit" class="btn btn-primary">submit</button>
</form>
