<?php

namespace App\Jobs;

use App\Services\KlaviyoService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class ImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user;
    private $file;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $file)
    {
        $this->user = $user;
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(KlaviyoService $klaviyoService)
    {
        try {
            echo "Processing file: {$this->file}...\n";

            $i = 0;
            $skipLine = true;
            $file = fopen(storage_path('import/' . $this->file), 'r');

            while (($line = fgetcsv($file)) !== false) {
                if($skipLine) {
                    $skipLine = false;
                    continue;
                }

                $data = array_combine(['first_name','last_name','email','phone'], $line);

                if(empty($data['email'])) {
                    continue;
                }

                $contact = $this->user->contacts()->where('email', $data['email'])->first();

                $validator = Validator::make($data, [
                    'last_name' => 'required|string|max:255',
                    'first_name' => 'required|string|max:255',
                    'phone' => 'required|string|max:20',
                    'email' => [
                        'required','email','max:255',
                        Rule::unique('contacts', 'email')->ignore($contact->id, 'id'),
                    ]
                ]);

                if($validator->fails()) {
                    continue;
                }

                $res = $klaviyoService->contactsImport($data, $contact, $this->user);

                if($res) {
                    $i++;
                }
            }

            fclose($file);

            echo "\nFinished and deleted {$this->file}, saved: {$i} items.\n";
        } catch (\Exception $e) {
            echo $e->getMessage();
            Log::error($e->getMessage().' in '.$e->getFile().':'.$e->getLine()."\n".$e->getTraceAsString());
        }

        if(Storage::disk('import')->exists($this->file)) {
            Storage::disk('import')->delete($this->file);
        }
    }
}
